<%@include file="resources/navbar.html"%>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>User Mgmt App</title>

  <%--    Stylesheets--%>
  <link href="${pageContext.request.contextPath}/resources/main.css" rel="stylesheet">

  <%--    Scripts--%>
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js" ></script>
  <script type="text/javascript" src="${pageContext.request.contextPath}/resources/main.js"></script>
</head>
<body>
  <div class="container">
    <div style="height: 100%">
      <div>
        <h2>User Management App</h2>
      </div>
      <div>
        <p class="info">
          This is a User Management App! <br>
          You can view a list of registered users or register a new user by clicking on the following buttons. <br>
          You can also use the menu bar items on the top of the page in order to navigate through the app.
        </p>
      </div>
      <div>
        <button class="btn" onclick="location.href='/display'">Display User List</button>
        <button class="btn" onclick="location.href='/register'">Register New User</button>
      </div>
    </div>
    <%@include file="resources/footer.html"%>
  </div>
</body>
</html>