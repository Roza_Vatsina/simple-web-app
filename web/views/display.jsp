<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../resources/navbar.html"%>
<html>
<head>
    <title>Display Users</title>

    <%--    Stylesheets--%>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.12.1/css/all.css" crossorigin="anonymous">
    <link href="${pageContext.request.contextPath}/resources/main.css" rel="stylesheet">

    <%--    Scripts--%>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js" ></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/main.js"></script>
</head>
<body>
    <div class="container">
        <div style="height: 100%">
            <div>
                <h2>Users List</h2>
            </div>
            <c:if test="${(not empty msg) and (not empty param.hasMsg)}">
                <p class="msg"><c:out value="${msg}" /></p>
            </c:if>
            <c:choose>
                <c:when test="${not empty users}">
                    <table class="center">
                        <thead>
                        <tr>
                            <th scope="col">Name</th>
                            <th scope="col">Surname</th>
                            <th style="min-width: 150px" scope="col">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="user" items="${users}">
                            <c:set var = "fullname" scope = "session" value = "${user.surname}${' '}${user.name}"/>
                            <tr class="user-item">
                                <td><c:out value='${user.name}' /></td>
                                <td><c:out value='${user.surname}' /></td>
                                <td>
                                    <button id="edit-btn" class="icon-btn tooltip" onclick="location.href='/edit?id=<c:out value='${user.id}' />'"><span class="tooltiptext">Edit User</span><i class="fa fa-edit"></i></button>
                                    <button id="delete-btn" class="icon-btn tooltip" onclick="location.href='/delete?id=<c:out value='${user.id}' />'"><span class="tooltiptext">Delete User</span><i class="fa fa-trash"></i></button>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </c:when>
                <c:otherwise>
                    <p class="msg">There are no users yet!</p>
                </c:otherwise>
            </c:choose>
        </div>
    <%@include file="../resources/footer.html"%>
    </div>
</body>
</html>