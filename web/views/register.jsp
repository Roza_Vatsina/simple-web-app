<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../resources/navbar.html"%>
<html>
<head>
    <title>Register User</title>

    <%--    Stylesheets--%>
    <link href="${pageContext.request.contextPath}/resources/main.css" rel="stylesheet">

    <%--    Scripts--%>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js" ></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/main.js"></script>
</head>
<body>
    <div class="container">
        <div>
            <div>
                <h2>Add a New User</h2>
            </div>
            <c:if test="${(not empty msg) and (not empty param.hasMsg)}">
                <p class="msg"><c:out value="${msg}" /></p>
            </c:if>
            <form action="/add" method="post">
                <label>Name <span class="required-field">(Required)</span></label>
                <input type="text" name="name" required="required">

                <label>Surname <span class="required-field">(Required)</span></label>
                <input type="text" name="surname" required="required">

                <label>Gender <span class="required-field">(Required)</span></label>
                <select name="gender" required="required">
                    <option value="" selected disabled>Select your gender...</option>
                    <option value="0">Male</option>
                    <option value="1">Female</option>
                    <option value="2">Other</option>
                </select>

                <label>Birthdate <span class="required-field">(Required)</span></label>
                <input type="date" name="birthdate" required="required">

                <h4>Work Address</h4>
                <label>Street Name</label>
                <input type="text" name="work-address-street">

                <label>Street Number</label>
                <input type="text" name="work-address-number">


                <h4>Home Address</h4>
                <label>Street Name</label>
                <input type="text" name="home-address-street">

                <label>Street Number</label>
                <input type="text" name="home-address-number">

                <br>
                <button class="btn" type="submit">Add User</button>
            </form>
        </div>
        <%@include file="../resources/footer.html"%>
    </div>
</body>
</html>
