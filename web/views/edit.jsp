<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../resources/navbar.html"%>
<html>
<head>
    <title>Edit User</title>

    <%--    Stylesheets--%>
    <link href="${pageContext.request.contextPath}/resources/main.css" rel="stylesheet">

    <%--    Scripts--%>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js" ></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/main.js"></script>
</head>
<body>
    <div class="container">
        <div>
            <div>
                <h2>Edit an existing user</h2>
            </div>
            <c:if test="${(not empty msg) and (not empty param.hasMsg)}">
                <p class="msg"><c:out value="${msg}" /></p>
            </c:if>
            <c:if test="${not empty user}">
            <form action="update" method="post">

                <input type="hidden" name="id" value="<c:out value='${user.id}' />" />

                <label>Name <span class="required-field">(Required)</span></label>
                <input type="text" name="name" value="<c:out value='${user.name}' />" required="required">

                <label>Surname <span class="required-field">(Required)</span></label>
                <input type="text" name="surname" value="<c:out value='${user.surname}' />" required="required">

                <label>Gender <span class="required-field">(Required)</span></label>
                <select name="gender" value="<c:out value='${user.gender}' />" required="required">
                    <option value="" disabled>Select your gender...</option>
                    <option value="0" ${user.gender == '0' ? 'selected' : ''}>Male</option>
                    <option value="1" ${user.gender == '1' ? 'selected' : ''}>Female</option>
                    <option value="2" ${user.gender == '2' ? 'selected' : ''}>Other</option>
                </select>

                <label>Birthdate <span class="required-field">(Required)</span></label>
                <input type="date" name="birthdate" value="<c:out value='${user.birthdate}' />" required="required">

                <h4>Work Address</h4>
                <input type="hidden" name="work-address-id" value="<c:out value='${workAddress.id}' />" />

                <label>Street Name</label>
                <input type="text" name="work-address-street" value="<c:out value='${workAddress.streetName}' />">

                <label>Street Number</label>
                <input type="text" name="work-address-number" value="<c:out value='${workAddress.streetNumber}' />">


                <h4>Home Address</h4>
                <input type="hidden" name="home-address-id" value="<c:out value='${homeAddress.id}' />" />

                <label>Street Name</label>
                <input type="text" name="home-address-street" value="<c:out value='${homeAddress.streetName}' />">

                <label>Street Number</label>
                <input type="text" name="home-address-number" value="<c:out value='${homeAddress.streetNumber}' />">

                <br>
                <button class="btn" type="submit">Update User</button>
                <button class="btn" type="submit" onclick="location.href='/display'">Back to Users List</button>
            </form>
            </c:if>
        </div>
        <%@include file="../resources/footer.html"%>
    </div>
</body>
</html>