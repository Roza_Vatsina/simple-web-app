jQuery(document).ready(function(){
    jQuery(function() {
        var path = window.location.href;
        jQuery('.nav-link').each(function() {
            if (this.href === path) {
                jQuery(this).parent().addClass('active');
            }
        });
    });
});