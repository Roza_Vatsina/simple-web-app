package app.controllers;

import app.daos.AddressDao;
import app.daos.UserDao;
import app.model.Address;
import app.model.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

public class UserMgmtServlet extends HttpServlet {
    AddressDao addressDao = new AddressDao();
    UserDao userDao = new UserDao();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getServletPath();

        switch (action) {
            case "/register":
                showRegistrationForm(request, response);
                break;
            case "/add":
                registerNewUser(request, response);
                break;
            case "/edit":
                showUserEditForm(request, response);
                break;
            case "/update":
                updateUser(request, response);
                break;
            case "/delete":
                deleteUser(request, response);
                break;
            case "/display":
                showListOfUsers(request, response);
                break;
        }
    }

    private void showRegistrationForm(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("views/register.jsp");
        requestDispatcher.forward(request, response);
    }

    private void registerNewUser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("name");
        String surname = request.getParameter("surname");
        int gender = Integer.valueOf(request.getParameter("gender"));
        LocalDate birthdate = LocalDate.parse(request.getParameter("birthdate"));

        String homeStreetName = request.getParameter("home-address-street");
        String homeStreetNumber = request.getParameter("home-address-number");
        String workStreetName = request.getParameter("work-address-street");
        String workStreetNumber = request.getParameter("work-address-number");
        Address homeAddress = new Address(homeStreetName, homeStreetNumber);
        Address workAddress = new Address(workStreetName, workStreetNumber);
        int homeAddressId = addressDao.addAddress(homeAddress);
        int workAddressId = addressDao.addAddress(workAddress);

        User user = new User(name, surname, gender, birthdate, homeAddressId, workAddressId);
        boolean isSuccess = userDao.addUser(user) && homeAddressId != -1 && workAddressId != -1;
        String msg, redirect;
        if (isSuccess) {
            msg = "The user was registered successfully!";
            redirect = "display";
        } else {
            msg = "There was a problem registering the user. Please try again.";
            redirect = "register";
        }
        msgHandling(request, response, msg, redirect);
    }

    private void deleteUser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        User user = userDao.getUserById(id);
        boolean isSuccess = userDao.deleteUser(id) &&
                addressDao.deleteAddress(user.getHomeAddressId()) &&
                addressDao.deleteAddress(user.getWorkAddressId());
        String msg = isSuccess ? "The user was deleted successfully!" : "There was a problem deleting the user. Please try again.";
        msgHandling(request, response, msg, "display");
    }

    private void showUserEditForm(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int userId = Integer.parseInt(request.getParameter("id"));
        User user = userDao.getUserById(userId);

        if(user == null) {
            msgHandling(request, response, "There was a problem while fetching the user. Please try again.", "display");
            return;
        }

        Address workAddress, homeAddress;
        if(user.getWorkAddressId() != 0) {
            workAddress =  addressDao.getAddressById(user.getWorkAddressId());
            if(workAddress == null) {
                msgHandling(request, response, "There was a problem while fetching the user details. Please try again.", "display");
                return;
            }
        }
        else {
            workAddress = new Address("", "");
            workAddress.setId(0);
        }

        if(user.getHomeAddressId() != 0) {
            homeAddress =  addressDao.getAddressById(user.getHomeAddressId());
            if(homeAddress == null) {
                msgHandling(request, response, "There was a problem while fetching the user. Please try again.", "display");
                return;
            }
        }
        else {
            homeAddress = new Address("", "");
            homeAddress.setId(0);
        }
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("views/edit.jsp");
        request.setAttribute("user", user);
        request.setAttribute("workAddress", workAddress);
        request.setAttribute("homeAddress", homeAddress);
        requestDispatcher.forward(request, response);
    }

    private void updateUser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int userId = Integer.parseInt(request.getParameter("id"));
        String name = request.getParameter("name");
        String surname = request.getParameter("surname");
        int gender = Integer.valueOf(request.getParameter("gender"));
        LocalDate birthdate = LocalDate.parse(request.getParameter("birthdate"));

        int homeAddressId = Integer.parseInt(request.getParameter("home-address-id"));
        String homeStreetName = request.getParameter("home-address-street");
        String homeStreetNumber = request.getParameter("home-address-number");
        Address homeAddress = new Address(homeStreetName, homeStreetNumber);
        if(homeAddressId != 0) homeAddress.setId(homeAddressId);
        homeAddressId = addressDao.updateAddress(homeAddress);

        int workAddressId = Integer.parseInt(request.getParameter("work-address-id"));
        String workStreetName = request.getParameter("work-address-street");
        String workStreetNumber = request.getParameter("work-address-number");
        Address workAddress = new Address(workStreetName, workStreetNumber);
        if(workAddressId != 0) workAddress.setId(workAddressId);
        workAddressId = addressDao.updateAddress(workAddress);

        User user = new User(name, surname, gender, birthdate, homeAddressId, workAddressId);
        user.setId(userId);
        boolean isSuccess = homeAddressId != -1 && workAddressId != -1 && userDao.updateUser(user);
        String msg, redirect;
        if (isSuccess){
            msg = "The user was updated successfully!";
            redirect = "display";
        } else {
            msg = "There was a problem updating the user. Please try again.";
            redirect = "edit";
        }
        msgHandling(request, response, msg, redirect);
    }

    private void showListOfUsers(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<User> userList = userDao.listAllUsers();
        request.setAttribute("users", userList);

        RequestDispatcher requestDispatcher = request.getRequestDispatcher("views/display.jsp");
        requestDispatcher.forward(request, response);
    }

    private void msgHandling(HttpServletRequest request, HttpServletResponse response, String msg, String redirect) throws IOException {
        HttpSession session = request.getSession();
        session.setAttribute("msg",msg);
        response.sendRedirect(redirect + "?hasMsg=true");
    }
}