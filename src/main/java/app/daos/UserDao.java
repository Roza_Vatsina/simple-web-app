package app.daos;

import app.model.User;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.transaction.Transactional;

public class UserDao {
	private static EntityManagerFactory factory = Persistence.createEntityManagerFactory("PERSISTENCE");;
	private EntityManager entityManager;

	public UserDao() {
		entityManager = factory.createEntityManager();
	}

	@Transactional
	public boolean addUser(User user) {
		try {
			entityManager.getTransaction().begin();
			entityManager.persist(user);
			entityManager.getTransaction().commit();
			return true;
		}
		catch (Exception e){
			e.printStackTrace();
			return false;
		}
	}

	public User getUserById(int userId) {
		try {
			User user = entityManager.find(User.class, userId);
			return user;
		}
		catch (Exception e){
			e.printStackTrace();
			return new User();
		}
	}

	public List<User> listAllUsers() {
		return (List<User>) entityManager.createNativeQuery(
				"SELECT * FROM Users", User.class)
				.getResultList();
	}

	public boolean deleteUser(int userId) {
		try {
			User user = entityManager.getReference(User.class, userId);
			entityManager.getTransaction().begin();
			entityManager.remove(user);
			entityManager.getTransaction().commit();
			return true;
		}
		catch (Exception e){
			e.printStackTrace();
			return false;
		}
	}

	public boolean updateUser(User user) {
		try {
			entityManager.getTransaction().begin();
			entityManager.merge(user);
			entityManager.flush();
			entityManager.getTransaction().commit();
			return true;
		}
		catch (Exception e){
			e.printStackTrace();
			return false;
		}
	}
}
