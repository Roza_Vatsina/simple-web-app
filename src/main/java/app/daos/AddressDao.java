package app.daos;

import app.model.Address;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

public class AddressDao {
	private static EntityManagerFactory factory = Persistence.createEntityManagerFactory("PERSISTENCE");
	private EntityManager entityManager;

	public AddressDao() {
		entityManager = factory.createEntityManager();
	}

	@Transactional
	public int addAddress(Address address) {
		try {
			if (address.getStreetName() == null && address.getStreetName().isEmpty() &&
					address.getStreetNumber() == null && address.getStreetNumber().isEmpty()) {
				return 0;
			} else {
				entityManager.getTransaction().begin();
				entityManager.persist(address);
				entityManager.flush();
				entityManager.getTransaction().commit();
				return address.getId();
			}
		}
		catch (Exception e){
			e.printStackTrace();
			return -1;
		}
	}

	public Address getAddressById(int addressId) {
		try {
			Address address = entityManager.find(Address.class, addressId);
			return address;
		}
		catch (Exception e){
			e.printStackTrace();
			return new Address();
		}
	}

	public List<Address> listAllAddress() {
		try {
			return (List<Address>) entityManager.createNativeQuery(
					"SELECT * FROM Addresses", Address.class)
					.getResultList();
		}
		catch (Exception e){
			e.printStackTrace();
			return new ArrayList<>();
		}
	}

	public boolean deleteAddress(int addressId) {
		try {
			if (addressId != 0) {
				Address address = entityManager.getReference(Address.class, addressId);
				entityManager.getTransaction().begin();
				entityManager.remove(address);
				entityManager.getTransaction().commit();
			}
			return true;
		}
		catch (Exception e){
			e.printStackTrace();
			return false;
		}
	}

	public int updateAddress(Address address) {
		try {
			if ((address.getStreetName() == null || address.getStreetName().isEmpty()) &&
					(address.getStreetNumber() == null || address.getStreetNumber().isEmpty())) {
				deleteAddress(address.getId());
				return 0;
			} else {
				entityManager.getTransaction().begin();
				Address updatedAddress = entityManager.merge(address);
				entityManager.flush();
				entityManager.getTransaction().commit();
				return updatedAddress.getId();
			}
		}
		catch (Exception e){
			e.printStackTrace();
			return -1;
		}
	}

}